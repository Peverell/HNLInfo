package hr.fer.ppij.a1hnlinfo.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;

import hr.fer.ppij.a1hnlinfo.Models.FootballTeam;
import hr.fer.ppij.a1hnlinfo.R;

/**
 * Created by AndrejKovar on 13.4.2017..
 */

public class TeamsAdapter extends RecyclerView.Adapter<TeamsAdapter.ViewHolder>{

    private final Context context;
    private final List<FootballTeam> teams;
    private final ClickListener clickListener;

    public TeamsAdapter(Context context, List<FootballTeam> teams, ClickListener clickListener){

        this.context = context;
        this.teams = teams;
        this.clickListener = clickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.rv_team, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.officialTeamName.setText(teams.get(position).getOfficialName());
        Picasso.with(context).load(teams.get(position).getLogoUrl()).into(holder.teamLogo);
    }

    @Override
    public int getItemCount() {
        return teams.size();
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.iv_team_logo)
        ImageView teamLogo;
        @BindView(R.id.tv_official_team_name)
        TextView officialTeamName;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

            if (clickListener != null) {
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        clickListener.onFootballTeamClicked(teams.get(getAdapterPosition()));
                    }
                });
            }
        }
    }

    public interface ClickListener {
        void onFootballTeamClicked(FootballTeam footballTeam);
    }
}
