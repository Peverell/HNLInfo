package hr.fer.ppij.a1hnlinfo.Networking;

import android.util.Base64;

import java.io.IOException;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import hr.fer.ppij.a1hnlinfo.Models.ExtendedFootballPlayer;
import hr.fer.ppij.a1hnlinfo.Models.ExtendedMatch;
import hr.fer.ppij.a1hnlinfo.Models.FootballPlayer;
import hr.fer.ppij.a1hnlinfo.Models.FootballTeam;
import hr.fer.ppij.a1hnlinfo.Models.Match;
import hr.fer.ppij.a1hnlinfo.Models.MatchResults;
import hr.fer.ppij.a1hnlinfo.Models.PlayerType;
import hr.fer.ppij.a1hnlinfo.Models.Round;
import hr.fer.ppij.a1hnlinfo.Models.TableEntry;

/**
 * The main class with all the methods for collecting 
 * HNL informations.
 */

    public class DataCollector {

    /**
     * Collects data about all past, current and future matches in the HNL league.
     * @return ArrayList object filled with Match objects.
     */
    public static ArrayList<Match> getMatches(ArrayList<FootballTeam> teams) throws IOException {
        ArrayList<Match> matches = new ArrayList<>();

        //getting html 
        String html = getHNLhtml(DataConstants.MATCHES_URL);

        //Isolating rounds from the relevant html section
        ArrayList<ArrayList<String>> lis = RegexUtils.matchAll(html, "colspan=\\\"7\\\"(.*?)(?:<tr><th|<\\/table)");


        int round;
        for (ArrayList<String> sl : lis) {
            String s = sl.get(0);

            //get round info from HTML
            round = Integer.valueOf(RegexUtils.getFirstMatch(s, "(\\d+).\\s*kolo"));

            //find every match in a single round
            ArrayList<ArrayList<String>> ms = RegexUtils.matchAll(s, "<tr><td>(\\d{2}.\\d{2}.\\d{4}.) (\\d\\d:\\d\\d)<.+?class=\\\"mjesto\\\">[\\\"\\']?([^,\\\"\\']+)[\\\"\\']?,\\s*([^<]+)<.+?<td>([^<]+)<\\/td><td>([^<]*)<\\/td><td>\\s*:\\s*<\\/td><td>([^<]*)<\\/td><td>([^<]+).+?(?:href=[\\\"\\']([^\\\"\\']+)|\\/td><td><\\/td><\\/tr>())");

            //Iterate through every match
            for (ArrayList<String> m : ms) {

                //Use date formatter for later converting String to Date
                DateFormat df = new SimpleDateFormat(DataConstants.MATCH_DATE_FORMAT);

                FootballTeam homeTeam = getTeamByName(m.get(4), teams);
                FootballTeam awayTeam = getTeamByName(m.get(7), teams);
                Integer homeGoals, awayGoals;

                //Initialize MatchResults object, set null value if game not played
                MatchResults result;
                try {
                    homeGoals = Integer.parseInt(m.get(5));
                    awayGoals = Integer.parseInt(m.get(6));
                    result = new MatchResults(homeGoals, awayGoals);
                } catch (NumberFormatException ex) {
                    result = new MatchResults();
                }

                try {
                    //initialize variables needed for match
                    Date date = df.parse(m.get(0).trim());
                    String matchCity = m.get(1);
                    String matchStadium = m.get(2);
                    String matchUrl = m.get(8);
                    //Add match to the list
                    matches.add(new Match(round, date, matchCity, matchStadium, homeTeam, awayTeam, result, matchUrl));
                } catch (ParseException ex) {
                    Logger.getLogger(DataCollector.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return matches;
    }
    
    /**
     * Gets the current table with all available information.
     * @return 
     */
    public static ArrayList<TableEntry> getTable(ArrayList<FootballTeam> teams) throws IOException {
        ArrayList<TableEntry> table_list = new ArrayList<>();
        
        String html = getHNLhtml(DataConstants.TABLE_URL);
        ArrayList<ArrayList<String>> entries = RegexUtils.matchAll(html, "<tr><td>(\\d+).<\\/td><td>([^<]+)<\\/td><td>(\\d+)<\\/td><td>(\\d+)<\\/td><td>(\\d+)<\\/td><td>(\\d+)<\\/td><td>(\\d+)<\\/td><td>(\\d+)<\\/td><td>([+-]\\d+)<\\/td><td>(\\d+)<\\/td><\\/tr>");
        for (ArrayList<String> e : entries) {
            int position = Integer.valueOf(e.get(0));
            FootballTeam team = getTeamByName(e.get(1), teams);
            int gamesPlayed = Integer.valueOf(e.get(2));
            int gamesWon = Integer.valueOf(e.get(3));
            int gamesDrawn = Integer.valueOf(e.get(4));
            int gamesLost = Integer.valueOf(e.get(5));
            int goalsFor = Integer.valueOf(e.get(6));
            int goalsAgainst = Integer.valueOf(e.get(7));
            int goalDifference = goalsFor - goalsAgainst;
            int points = Integer.valueOf(e.get(9));
            TableEntry entry = new TableEntry(position, team, gamesPlayed, gamesWon, gamesDrawn, gamesLost, goalsFor, goalsAgainst, goalDifference, points);
            table_list.add(entry);
        }
        Collections.sort(table_list);

        return table_list;
    }


    /**
     * Returns the FootballTeam object based on the provided name of the team.
     * @param name
     * @param teams
     * @return
     */
    public static FootballTeam getTeamByName(String name, ArrayList<FootballTeam> teams) {
        for (FootballTeam t : teams) {

            if (t.getFootballTeamName().equals(name) || t.getOfficialName().equals(name) || t.getFootballTeamName().contains(name) || t.getOfficialName().contains(name)) {
                if (name.equals("Split") && (t.getFootballTeamName().contains("Hajduk") || t.getOfficialName().contains("Hajduk"))){
                    continue;
                }
                return t;
            }
        }
        return new FootballTeam(name);

    }


    /**
     * Collects rich data about every team in the league.
     * @return
     * @throws IOException
     */
    public static ArrayList<FootballTeam> getTeams() throws IOException {
        ArrayList<FootballTeam> teams = new ArrayList<>();
        ArrayList<String> logos = new ArrayList<>();

        String html = Browser.getHTML(DataConstants.DOMAIN);
        ArrayList<ArrayList<String>> infos = RegexUtils.matchAll(html, "a title=[\\\"\\']([^\\\"\\']+)[\\\"\\'].+?href=[\\\"\\']([^\\\"\\']+)[\\\"\\']><img.+?src=[\\\"\\']([^\\\"\\']+)[\\\"\\']");
        for (ArrayList<String> i : infos) {
            ArrayList<FootballPlayer> teamPlayers = new ArrayList<>();
            String name = i.get(0);
            String url = DataConstants.DOMAIN + i.get(1);
            String logoUrl = DataConstants.DOMAIN + i.get(2);

            //add logo url to the list
            logos.add(logoUrl);

            //get html of a team
            String html2 = getHNLhtml(url);

            //find all players
            teamPlayers = RezultatiCollector.getPlayers(name);


            ArrayList<String> extendedInfo = RegexUtils.matchPattern(html2, "class=[\\\"\\']page[\\\"\\']>([^<]+).+?Adresa: \\s*([^<]+).+?Web:.+?href=[\\\"\\']([^\\\"\\']+).+?Osnovan:\\s*(\\d+).+?Stadion:\\s*([^\\(]+)\\(([^\\)]+).+?Nadimak:\\s*([^<]+)");
            String teamName = RegexUtils.getFirstMatch(html2, "<h2><b>\\s*(.+?)\\s*<\\/b>");
            String officialName = extendedInfo.get(0).trim();
            String address = extendedInfo.get(1);
            String website = extendedInfo.get(2);
            int founded = Integer.valueOf(extendedInfo.get(3));
            String stadium = extendedInfo.get(4).trim();
            int stadiumCapacity = Integer.valueOf((extendedInfo.get(5).replace(".","")));
            String nickname = extendedInfo.get(6);

            String email = RegexUtils.getFirstMatch(html2, "e-mail.*?:(?:<.+?>)?\\s*([^<]+)");
            String coach = RegexUtils.getFirstMatch(html2, "Trener[^:]+:.+?(?:<span>)?\\s*([^<)]+)<\\/");


            //creating FootballTeam object
            FootballTeam team = new FootballTeam(teamName, officialName, url, logoUrl, email, website, address,
                    coach, stadium, stadiumCapacity, founded, nickname, teamPlayers);

            teams.add(team);

        }

        return teams;
    }

    public static ExtendedMatch getExtendedMatch(Match match) throws IOException {
        ArrayList<ExtendedFootballPlayer> homePlayers = new ArrayList<>();
        ArrayList<ExtendedFootballPlayer> awayPlayers = new ArrayList<>();


        String html = getHNLhtml(DataConstants.DOMAIN + match.getMatchUrl());
        String match_description = RegexUtils.getFirstMatch(html, "Gledatelj.\\s*:\\s*(\\d+)");

        ArrayList<ArrayList<String>> playerPatterns = RegexUtils.matchAll(html, "(<td>\\d+.+?<\\/td>)");

        int counter = 0;

        for (ArrayList<String> ps : playerPatterns) {
            String p = ps.get(0);
            counter += 1;
            int number = Integer.valueOf(RegexUtils.getFirstMatch(p, "<td>(\\d+)"));
            String name = RegexUtils.getFirstMatch(p, "<td>([^\\d<]+)<");

            int yellow = Integer.valueOf(RegexUtils.getFirstMatch(p, "class=[\\\"\\']zuti.+?(\\d+)\\'").replace("","0"));
            int secondYellow = Integer.valueOf(RegexUtils.getFirstMatch(p, "class=[\\\"\\']drugizuti.+?(\\d+)\\'").replace("","0"));
            int red = Integer.valueOf(RegexUtils.getFirstMatch(p, "class=[\\\"\\']crveni.+?(\\d+)\\'").replace("","0"));
            int out = Integer.valueOf(RegexUtils.getFirstMatch(p, "class=[\\\"\\']out.+?(\\d+)\\'").replace("","0"));
            int in = Integer.valueOf(RegexUtils.getFirstMatch(p, "class=[\\\"\\']in.+?(\\d+)\\'").replace("","0"));

            String playerType;
            if (counter <= 22) {
                playerType = PlayerType.STARTER;
            }
            else {
                playerType = PlayerType.SUBSTITUTE;
            }

            ArrayList<ArrayList<String>> goalMs = RegexUtils.matchAll(p, "class=[\\\"\\']gol.+?(\\d+)\\'");
            ArrayList<Integer> goals = new ArrayList<>();
            for (ArrayList<String> g : goalMs) {
                goals.add(Integer.valueOf(g.get(0).replace("","0")));
            }
            FootballPlayer player = getPlayerByName(match, name);
            ExtendedFootballPlayer extPlayer = new ExtendedFootballPlayer(player, number, goals, yellow, secondYellow, red, out, in, playerType );

            if (counter % 2 == 0){
                awayPlayers.add(extPlayer);
            }
            else {
                homePlayers.add(extPlayer);
            }


        }

        ExtendedMatch extMatch = new ExtendedMatch(match,match_description,homePlayers,awayPlayers);


        return extMatch;

    }

    private static FootballPlayer getPlayerByName(Match match, String name) {
        FootballTeam home = match.getHomeTeam();
        FootballTeam away = match.getAwayTeam();
        List<FootballPlayer> players = home.getPlayers();

        players.addAll(away.getPlayers());

        String firstName = name.split(" ")[name.split(" ").length-1];
        String lastName = name.replace(firstName, ""); lastName.trim();

        for (FootballPlayer p : players) {
            String name2 = p.getPlayerFirstName() + " " + p.getPlayerLastName();
            if ((p.getPlayerFirstName().equals(firstName) && p.getPlayerLastName().equals(lastName) || name2.contains(name) || name2.equals(name) || name.equals(name2))){
                return p;
            }
        }

        return null;



    }

    public static List<Round> getRounds() throws IOException {
        ArrayList<Round> rounds = new ArrayList<>();
        String html = Browser.getHTML(DataConstants.REZULTATI_URL);
        int last_round = Integer.valueOf(RegexUtils.getFirstMatch(html, "(\\d+).\\s+kolo"));
        for (int i=last_round; i>0; i--) {
            rounds.add(new Round(i, String.format("%d. KOLO", i)));
        }

        return rounds;
    }

    public static ArrayList<Match> getMatchesByRound(Round round, ArrayList<Match> matches) {
        ArrayList<Match> myMatches = new ArrayList<>();
        for (Match m : matches) {
            if (m.getMatchWeek() == round.getNumber()){
                myMatches.add(m);
            }
        }
        return myMatches;
    }

    /**
     * Inner helper method for isolating and
     * decoding the needed part of HNL page.
     * @param url
     * @return
     *
     */
    private static String getHNLhtml (String url) throws IOException {

        String html = Browser.getHTML(url);
        String encoded = RegexUtils.getFirstMatch(html, "id=[\\\"\\'].+?VIEWSTATE.+?value=[\\\"\\']([^\\\"\\']+)");
        byte[] decoded = Base64.decode(encoded, Base64.DEFAULT);
        return new String(decoded, Charset.defaultCharset());
    }
}
