package hr.fer.ppij.a1hnlinfo.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import hr.fer.ppij.a1hnlinfo.Activites.FootballTeamActivity;
import hr.fer.ppij.a1hnlinfo.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class TeamInfoFragment extends Fragment {

    public final static String TAG = "TeamInfoFragmentTag";

    @BindView(R.id.iv_team_logo)
    ImageView ivTeamLogo;

    @BindView(R.id.tv_team_name)
    TextView tvTeamName;

    @BindView(R.id.tv_team_stadium)
    TextView tvTeamStadium;

    @BindView(R.id.tv_team_founded)
    TextView tvTeamFounded;

    @BindView(R.id.tv_team_website)
    TextView tvTeamWebsite;

    @BindView(R.id.tv_team_adress)
    TextView tvTeamAdress;

    @BindView(R.id.tv_team_coach)
    TextView tvTeamCoach;



    public TeamInfoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_team_info, container, false);
        ButterKnife.bind(this, view);
        viewInitialization();
        return view;
    }

    private void viewInitialization(){

        Picasso.with(getContext()).load(((FootballTeamActivity)getActivity()).getFootballTeam().getLogoUrl()).into(ivTeamLogo);
        tvTeamName.setText(((FootballTeamActivity)getActivity()).getFootballTeam().getOfficialName());
        tvTeamStadium.setText(((FootballTeamActivity)getActivity()).getFootballTeam().getStadium() + " (kapacitet: " + ((FootballTeamActivity)getActivity()).getFootballTeam().getStadiumCapacity() + ")");
        tvTeamFounded.setText("Osnovan: " + ((FootballTeamActivity)getActivity()).getFootballTeam().getFounded() + "");
        tvTeamAdress.setText(((FootballTeamActivity)getActivity()).getFootballTeam().getAddress());
        tvTeamWebsite.setText(((FootballTeamActivity)getActivity()).getFootballTeam().getWebsite());
        tvTeamCoach.setText(((FootballTeamActivity)getActivity()).getFootballTeam().getCoach());
    }
}
