package hr.fer.ppij.a1hnlinfo.Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by natko on 25.04.17..
 */

public class ExtendedFootballPlayer implements Parcelable {
    private FootballPlayer player;
    private int number;
    private ArrayList<Integer> goals;
    private int yellowCard;
    private int secondYellowCard;
    private int redCard;
    private int out;
    private int in;
    private String playerType;

    public ExtendedFootballPlayer(FootballPlayer player, int number, ArrayList<Integer> goals, int yellowCard, int secondYellowCard, int redCard, int out, int in, String playerType) {
        this.player = player;
        this.number = number;
        this.goals = goals;
        this.yellowCard = yellowCard;
        this.secondYellowCard = secondYellowCard;
        this.redCard = redCard;
        this.out = out;
        this.in = in;
        this.playerType = playerType;
    }

    public ExtendedFootballPlayer (Parcel in) {
        player = in.readParcelable(getClass().getClassLoader());
        number = in.readInt();
        goals = in.readArrayList(getClass().getClassLoader());
        yellowCard = in.readInt();
        secondYellowCard = in.readInt();
        redCard = in.readInt();
        out = in.readInt();
        this.in = in.readInt();
        playerType = in.readString();

    }

    public static final Creator<ExtendedFootballPlayer> CREATOR = new Creator<ExtendedFootballPlayer>() {
        @Override
        public ExtendedFootballPlayer createFromParcel(Parcel in) {
            return new ExtendedFootballPlayer(in);
        }

        @Override
        public ExtendedFootballPlayer[] newArray(int size) {
            return new ExtendedFootballPlayer[size];
        }
    };

    public FootballPlayer getPlayer() {
        return player;
    }

    public void setPlayer(FootballPlayer player) {
        this.player = player;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public ArrayList<Integer> getGoals() {
        return goals;
    }

    public void setGoals(ArrayList<Integer> goals) {
        this.goals = goals;
    }

    public int getYellowCard() {
        return yellowCard;
    }

    public void setYellowCard(int yellowCard) {
        this.yellowCard = yellowCard;
    }

    public int getSecondYellowCard() {
        return secondYellowCard;
    }

    public void setSecondYellowCard(int secondYellowCard) {
        this.secondYellowCard = secondYellowCard;
    }

    public int getRedCard() {
        return redCard;
    }

    public void setRedCard(int redCard) {
        this.redCard = redCard;
    }

    public int getOut() {
        return out;
    }

    public void setOut(int out) {
        this.out = out;
    }

    public int getIn() {
        return in;
    }

    public void setIn(int in) {
        this.in = in;
    }

    public String getPlayerType() {
        return playerType;
    }

    public void setPlayerType(String playerType) {
        this.playerType = playerType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(player,0);
        parcel.writeInt(number);
        parcel.writeList(goals);
        parcel.writeInt(yellowCard);
        parcel.writeInt(secondYellowCard);
        parcel.writeInt(redCard);
        parcel.writeInt(out);
        parcel.writeInt(in);
        parcel.writeString(playerType);
    }
}
