package hr.fer.ppij.a1hnlinfo.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by AndrejKovar on 28.3.2017..
 * Modified by natko on 4.4.2017.
 */

public class FootballPlayer implements Parcelable {

    private String playerFirstName;
    private String playerLastName;
    private int appearances;
    private int years;
    private int goals;
    private int yellowCards;
    private int redCards;
    private String playerPosition;

    public FootballPlayer(String playerFirstName, String playerLastName, int appearances,
                          int years, int goals, int yellowCards, int redCards, String playerPosition) {
        this.playerFirstName = playerFirstName;
        this.playerLastName = playerLastName;
        this.appearances = appearances;
        this.years = years;
        this.goals = goals;
        this.yellowCards = yellowCards;
        this.redCards = redCards;
        this.playerPosition = playerPosition;
    }

    protected FootballPlayer(Parcel in) {
        playerFirstName = in.readString();
        playerLastName = in.readString();
        appearances = in.readInt();
        years = in.readInt();
        goals = in.readInt();
        yellowCards = in.readInt();
        redCards = in.readInt();
        playerPosition = in.readString();
    }

    public static final Creator<FootballPlayer> CREATOR = new Creator<FootballPlayer>() {
        @Override
        public FootballPlayer createFromParcel(Parcel in) {
            return new FootballPlayer(in);
        }

        @Override
        public FootballPlayer[] newArray(int size) {
            return new FootballPlayer[size];
        }
    };

    public String getPlayerFirstName() {
        return playerFirstName;
    }

    public void setPlayerFirstName(String playerFirstName) {
        this.playerFirstName = playerFirstName;
    }

    public String getPlayerLastName() {
        return playerLastName;
    }

    public void setPlayerLastName(String playerLastName) {
        this.playerLastName = playerLastName;
    }

    public int getAppearances() {
        return appearances;
    }

    public void setAppearances(int appearances) {
        this.appearances = appearances;
    }

    public int getyears() {
        return years;
    }

    public void setyears(int years) {
        this.years = years;
    }

    public int getGoals() {
        return goals;
    }

    public void setGoals(int goals) {
        this.goals = goals;
    }

    public int getYellowCards() {
        return yellowCards;
    }

    public void setYellowCards(int yellowCards) {
        this.yellowCards = yellowCards;
    }

    public int getRedCards() {
        return redCards;
    }

    public void setRedCards(int redCards) {
        this.redCards = redCards;
    }

    public String getPlayerPosition() {
        return playerPosition;
    }

    public void setPlayerPosition(String playerPosition) {
        this.playerPosition = playerPosition;
    }

    @Override
    public String toString() {
        return "FootballPlayer{" +
                "playerFirstName='" + playerFirstName + '\'' +
                ", playerLastName='" + playerLastName + '\'' +
                ", appearances=" + appearances +
                ", years=" + years +
                ", goals=" + goals +
                ", yellowCards=" + yellowCards +
                ", redCards=" + redCards +
                ", playerPosition=" + playerPosition +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(playerFirstName);
        parcel.writeString(playerLastName);
        parcel.writeInt(appearances);
        parcel.writeInt(years);
        parcel.writeInt(goals);
        parcel.writeInt(yellowCards);
        parcel.writeInt(redCards);
        parcel.writeString(playerPosition);
    }
}
