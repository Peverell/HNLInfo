package hr.fer.ppij.a1hnlinfo.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import hr.fer.ppij.a1hnlinfo.Models.Match;
import hr.fer.ppij.a1hnlinfo.R;

/**
 * Created by natko on 06.05.17..
 */

public class MatchAdapter extends RecyclerView.Adapter<MatchAdapter.ViewHolder>{

    private Context _context;
    private final ArrayList<Match> matches;

    public MatchAdapter(Context context, List<Match> matches) {
        this._context = context;
        this.matches = (ArrayList<Match>) matches;
    }

    @Override
    public MatchAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(_context).inflate(R.layout.rv_match, parent, false);
        return new MatchAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MatchAdapter.ViewHolder holder, int position) {
        String awayG;
        Integer ag = matches.get(position).getMatchResults().getAwayTeamGoals();
        awayG = String.valueOf(ag);
        if (ag == null){
            awayG = "";
        }

        String homeG;
        Integer hg = matches.get(position).getMatchResults().getHomeTeamGoals();
        homeG = String.valueOf(hg);
        if (hg == null){
            homeG = "";
        }


        holder.awayTeamGoals.setText(awayG);
        holder.homeTeamGoals.setText(homeG);

        holder.homeTeamName.setText(matches.get(position).getHomeTeam().getFootballTeamName());
        holder.awayTeamName.setText(matches.get(position).getAwayTeam().getFootballTeamName());

        DateFormat df = new SimpleDateFormat("dd.MM.yyyy.");
        String info = df.format(matches.get(position).getMatchDate());
        holder.matchDate.setText(info);

        holder.matchStadium.setText(matches.get(position).getMatchStadium());

        Picasso.with(_context).load(matches.get(position).getHomeTeam().getLogoUrl()).into(holder.homeTeamLogo);
        Picasso.with(_context).load(matches.get(position).getAwayTeam().getLogoUrl()).into(holder.awayTeamLogo);
    }

    @Override
    public int getItemCount() {
        return matches.size();
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_homeTeam_logo)
        ImageView homeTeamLogo;
        @BindView(R.id.tv_homeTeam_name)
        TextView homeTeamName;
        @BindView(R.id.homeTeamGoals)
        TextView homeTeamGoals;

        @BindView(R.id.iv_awayTeam_logo)
        ImageView awayTeamLogo;
        @BindView(R.id.tv_awayTeam_name)
        TextView awayTeamName;
        @BindView(R.id.awayTeamGoals)
        TextView awayTeamGoals;

        @BindView(R.id.tv_match_date)
        TextView matchDate;
        @BindView(R.id.tv_match_stadium)
        TextView matchStadium;


        public ViewHolder(View view) {

            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
