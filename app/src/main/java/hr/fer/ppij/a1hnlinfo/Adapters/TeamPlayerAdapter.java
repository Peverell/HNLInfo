package hr.fer.ppij.a1hnlinfo.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import hr.fer.ppij.a1hnlinfo.Models.FootballPlayer;
import hr.fer.ppij.a1hnlinfo.Models.PlayerPosition;
import hr.fer.ppij.a1hnlinfo.R;

import java.util.List;

public class TeamPlayerAdapter extends RecyclerView.Adapter<TeamPlayerAdapter.ViewHolder> {

    private final List<FootballPlayer> footballPlayers;
    private Context context;

    public TeamPlayerAdapter(List<FootballPlayer> footballPlayers, Context context) {
        this.footballPlayers = footballPlayers;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.rv_player, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        holder.mItem = footballPlayers.get(position);

        holder.tvPlayerPosition.setText(footballPlayers.get(position).getPlayerPosition());
        switch (holder.tvPlayerPosition.getText().toString()){
            case PlayerPosition.GOALKEEPER:
                holder.tvPlayerPosition.setTextColor(context.getResources().getColor(R.color.colorGK));
                break;
            case PlayerPosition.DEFENDER:
                holder.tvPlayerPosition.setTextColor(context.getResources().getColor(R.color.colorDF));
                break;
            case PlayerPosition.MIDFIELDER:
                holder.tvPlayerPosition.setTextColor(context.getResources().getColor(R.color.colorMF));
                break;
            case PlayerPosition.STRIKER:
                holder.tvPlayerPosition.setTextColor(context.getResources().getColor(R.color.colorST));
                break;
            default:
                holder.tvPlayerPosition.setTextColor(context.getResources().getColor(R.color.white));
        }

        holder.tvPlayerName.setText(footballPlayers.get(position).getPlayerFirstName() + " " + footballPlayers.get(position).getPlayerLastName());
        holder.tvPlayerGoals.setText(String.valueOf(footballPlayers.get(position).getGoals()));
        holder.tvPlayerYellowCards.setText(String.valueOf(footballPlayers.get(position).getYellowCards()));
        holder.tvPlayerRedCards.setText(String.valueOf(footballPlayers.get(position).getRedCards()));
    }

    @Override
    public int getItemCount() {
        return footballPlayers.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView tvPlayerPosition;
        public final TextView tvPlayerName;
        public final TextView tvPlayerGoals;
        public final TextView tvPlayerYellowCards;
        public final TextView tvPlayerRedCards;
        public FootballPlayer mItem;

        public ViewHolder(View view) {
            super(view);
            tvPlayerPosition = (TextView) view.findViewById(R.id.tv_player_position);
            tvPlayerName = (TextView) view.findViewById(R.id.tv_player_name);
            tvPlayerGoals = (TextView) view.findViewById(R.id.tv_player_goals);
            tvPlayerYellowCards = (TextView) view.findViewById(R.id.tv_player_yellow_card);
            tvPlayerRedCards = (TextView) view.findViewById(R.id.tv_player_red_cards);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + tvPlayerName.getText() + "'";
        }
    }
}
