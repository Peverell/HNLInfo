package hr.fer.ppij.a1hnlinfo.Activites;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import butterknife.BindView;
import butterknife.ButterKnife;
import hr.fer.ppij.a1hnlinfo.Adapters.FootballTeamPagerAdapter;
import hr.fer.ppij.a1hnlinfo.Models.FootballTeam;
import hr.fer.ppij.a1hnlinfo.R;

public class FootballTeamActivity extends AppCompatActivity {

    public final static String FOOTBALLTEAM_ACTIVITY_TAG = "FootballTeamActivity";

    private FootballTeam footballTeam;

    @BindView(R.id.football_team_toolbar)
    Toolbar footballTeamToolbar;

    @BindView(R.id.football_team_tab_layout)
    TabLayout footballTeamTabLayout;

    @BindView(R.id.football_team_pager)
    ViewPager footballTeamViewPager;


    @SuppressWarnings("ConstantConditions")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_football_team);
        ButterKnife.bind(this);

        footballTeam = getIntent().getParcelableExtra(FOOTBALLTEAM_ACTIVITY_TAG);

        setSupportActionBar(footballTeamToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setSupportActionBar(footballTeamToolbar);
        getSupportActionBar().setTitle(footballTeam.getFootballTeamName());

        footballTeamTabLayout.addTab(footballTeamTabLayout.newTab().setText("O klubu"));
        footballTeamTabLayout.addTab(footballTeamTabLayout.newTab().setText("Momčad"));
        footballTeamTabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        FootballTeamPagerAdapter footballTeamPagerAdapter = new FootballTeamPagerAdapter(
                getSupportFragmentManager(),
                footballTeamTabLayout.getTabCount()
        );
        footballTeamViewPager.setAdapter(footballTeamPagerAdapter);
        footballTeamViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(footballTeamTabLayout));

        footballTeamTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                footballTeamViewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        footballTeam = getIntent().getParcelableExtra(FOOTBALLTEAM_ACTIVITY_TAG);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public static Intent getIntent(Context context, FootballTeam footballTeam) {

        Intent intent = new Intent(context, FootballTeamActivity.class);
        intent.putExtra(FOOTBALLTEAM_ACTIVITY_TAG, footballTeam);
        return intent;
    }

    public FootballTeam getFootballTeam(){
        return footballTeam;
    }
}
