package hr.fer.ppij.a1hnlinfo.Fragments;


import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import hr.fer.ppij.a1hnlinfo.Activites.MainActivity;
import hr.fer.ppij.a1hnlinfo.Models.TableEntry;
import hr.fer.ppij.a1hnlinfo.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TableFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TableFragment extends Fragment {

    public static final String TAG = "table_fragment";
    public static final int ID = 1;

    @Nullable
    @BindView(R.id.TL_fragment_table)
    TableLayout portraitTable;

    @Nullable
    @BindView(R.id.TL_fragment_table_land)
    TableLayout landscapeTable;

    public TableFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment TableFragment.
     */
    public static TableFragment newInstance() {
        TableFragment fragment = new TableFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_table, container, false);
        ButterKnife.bind(this, view);
        tableInit();
        return view;
    }

    private void tableInit(){

        Context context = getContext();
        for(TableEntry tempTableEntry : ((MainActivity)getActivity()).getTable()){

            TextView teamPosition = createTextView(context, String.valueOf(tempTableEntry.getPosition()) + ".", true, false);
            setTeamPositionBackground(teamPosition, tempTableEntry.getPosition());
            ImageView teamLogo = new ImageView(context);
            teamLogo.setLayoutParams(new TableRow.LayoutParams(Math.round(teamPosition.getTextSize() + 10), Math.round(teamPosition.getTextSize()) + 10));
            Picasso.with(context).load(tempTableEntry.getTeam().getLogoUrl()).into(teamLogo);
            TextView teamName = createTextView(context, tempTableEntry.getTeam().getFootballTeamName(), false, false);
            TextView teamPlayed = createTextView(context, String.valueOf(tempTableEntry.getGamesPlayed()), true, false);
            TextView teamGamesWon = createTextView(context, String.valueOf(tempTableEntry.getGamesWon()), true, false);
            TextView teamGamesLost = createTextView(context, String.valueOf(tempTableEntry.getGamesLost()), true, false);
            TextView teamGameDrawn = createTextView(context, String.valueOf(tempTableEntry.getGamesDrawn()), true, false);
            TextView teamGoalsFor = createTextView(context, String.valueOf(tempTableEntry.getGoalsFor()), true, false);
            TextView teamGoalsAgainst = createTextView(context, String.valueOf(tempTableEntry.getGoalsAgainst()), true, false);
            TextView teamGoalsDifference = createTextView(context, String.valueOf(tempTableEntry.getGoalDifference()), true, false);
            TextView teamPoints = createTextView(context, String.valueOf(tempTableEntry.getPoints()), true, true);

            if(getActivity().getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){

                TableRow newPortraitRow = new TableRow(context);
                TableLayout.LayoutParams tableRowLayoutParams = new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
                tableRowLayoutParams.setMargins(0, 10, 0, 10);
                newPortraitRow.setLayoutParams(tableRowLayoutParams);
                newPortraitRow.addView(teamPosition);
                newPortraitRow.addView(teamLogo);
                newPortraitRow.addView(teamName);
                newPortraitRow.addView(teamPlayed);
                newPortraitRow.addView(teamGoalsDifference);
                newPortraitRow.addView(teamPoints);
                portraitTable.addView(newPortraitRow);

            } else{

                TableRow newLandscapeRow = new TableRow(context);
                TableRow.LayoutParams tableRowLayoutParams = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
                tableRowLayoutParams.setMargins(0, 10, 0, 10);
                newLandscapeRow.setLayoutParams(tableRowLayoutParams);
                newLandscapeRow.addView(teamPosition);
                newLandscapeRow.addView(teamLogo);
                newLandscapeRow.addView(teamName);
                newLandscapeRow.addView(teamPlayed);
                newLandscapeRow.addView(teamGamesWon);
                newLandscapeRow.addView(teamGameDrawn);
                newLandscapeRow.addView(teamGamesLost);
                newLandscapeRow.addView(teamGoalsFor);
                newLandscapeRow.addView(teamGoalsAgainst);
                newLandscapeRow.addView(teamGoalsDifference);
                newLandscapeRow.addView(teamPoints);
                landscapeTable.addView(newLandscapeRow);
            }
        }
    }

    private TextView createTextView(Context context, String text, boolean needCenterGravity, boolean needBackground) {

        TextView textView = new TextView(context);
        textView.setText(text);
        textView.setGravity(needCenterGravity ? Gravity.CENTER : Gravity.START);

        if (needBackground) {
            textView.setBackground(getResources().getDrawable(R.drawable.player_position_background));
            textView.setTextColor(getResources().getColor(R.color.white));
            textView.setTypeface(null, Typeface.BOLD);
        }

        return textView;
    }

    private void setTeamPositionBackground(TextView textView, int tempPosition){

        if (tempPosition == 1){

            textView.setBackground(getResources().getDrawable(R.drawable.blue_rounded_background));
            textView.setTextColor(getResources().getColor(R.color.white));

        } else if (tempPosition == 2 || tempPosition == 3 || tempPosition == 4){

            textView.setBackground(getResources().getDrawable(R.drawable.orange_rounded_background));
            textView.setTextColor(getResources().getColor(R.color.white));

        } else if (tempPosition == 10){

            textView.setBackground(getResources().getDrawable(R.drawable.red_rounded_background));
            textView.setTextColor(getResources().getColor(R.color.white));

        }

        textView.setTypeface(null, Typeface.BOLD);
    }
}
