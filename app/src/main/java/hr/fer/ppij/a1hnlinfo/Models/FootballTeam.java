package hr.fer.ppij.a1hnlinfo.Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by AndrejKovar on 28.3.2017..
 */

public class FootballTeam implements Parcelable{

    private String footballTeamName;
    private String officialName;
    private String url;
    private String logoUrl;
    private String email;
    private String website;
    private String address;
    private String coach;
    private String stadium;
    private int stadiumCapacity;
    private int founded;
    private String nickname;

    private ArrayList<FootballPlayer> players;


    public FootballTeam(String footballTeamName) {
        this.footballTeamName = footballTeamName;
        this.officialName = footballTeamName;
        this.url = "";
        this.logoUrl = "";
        this.email = "";
        this.website = "";
        this.address = "";
        this.coach = "";
        this.stadium = "";
        this.stadiumCapacity = 0;
        this.founded = 0;
        this.nickname = "";
        this.players = null;
    }

    public FootballTeam(FootballTeam footballTeam) {
        this.footballTeamName = footballTeam.getFootballTeamName();
        this.officialName = footballTeam.getOfficialName();
        this.url = footballTeam.getUrl();
        this.logoUrl = footballTeam.getLogoUrl();
        this.email = footballTeam.getEmail();
        this.website = footballTeam.getWebsite();
        this.address = footballTeam.getAddress();
        this.coach = footballTeam.getCoach();
        this.stadium = footballTeam.getStadium();
        this.stadiumCapacity = footballTeam.getStadiumCapacity();
        this.founded = footballTeam.getFounded();
        this.nickname = footballTeam.getNickname();
        this.players = footballTeam.getPlayers();
    }

    public FootballTeam(String footballTeamName, String officialName, String url, String logoUrl, String email, String website, String address, String coach, String stadium, int stadiumCapacity, int founded, String nickname, ArrayList<FootballPlayer> players) {
        this.footballTeamName = footballTeamName;
        this.officialName = officialName;
        this.url = url;
        this.logoUrl = logoUrl;
        this.email = email;
        this.website = website;
        this.address = address;
        this.coach = coach;
        this.stadium = stadium;
        this.stadiumCapacity = stadiumCapacity;
        this.founded = founded;
        this.nickname = nickname;
        this.players = players;
    }



    protected FootballTeam(Parcel in) {
        footballTeamName = in.readString();
        officialName = in.readString();
        url = in.readString();
        logoUrl = in.readString();
        email = in.readString();
        website = in.readString();
        address = in.readString();
        coach = in.readString();
        stadium = in.readString();
        stadiumCapacity = in.readInt();
        founded = in.readInt();
        nickname = in.readString();
        players = in.createTypedArrayList(FootballPlayer.CREATOR);
    }

    public static final Creator<FootballTeam> CREATOR = new Creator<FootballTeam>() {
        @Override
        public FootballTeam createFromParcel(Parcel in) {
            return new FootballTeam(in);
        }

        @Override
        public FootballTeam[] newArray(int size) {
            return new FootballTeam[size];
        }
    };

    @Override
    public String toString() {
        return "FootballTeam{" +
                "footballTeamName='" + footballTeamName + '\'' +
                ", officialName='" + officialName + '\'' +
                ", url='" + url + '\'' +
                ", logoUrl='" + logoUrl + '\'' +
                ", email='" + email + '\'' +
                ", website='" + website + '\'' +
                ", address='" + address + '\'' +
                ", coach='" + coach + '\'' +
                ", stadium='" + stadium + '\'' +
                ", stadiumCapacity=" + stadiumCapacity +
                ", founded=" + founded +
                ", nickname='" + nickname + '\'' +
                ", players=" + players +
                '}';
    }

    public String getFootballTeamName() {
        return footballTeamName;
    }

    public void setFootballTeamName(String footballTeamName) {
        this.footballTeamName = footballTeamName;
    }

    public String getOfficialName() {
        return officialName;
    }

    public void setOfficialName(String officialName) {
        this.officialName = officialName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCoach() {
        return coach;
    }

    public void setCoach(String coach) {
        this.coach = coach;
    }

    public String getStadium() {
        return stadium;
    }

    public void setStadium(String stadium) {
        this.stadium = stadium;
    }

    public int getStadiumCapacity() {
        return stadiumCapacity;
    }

    public void setStadiumCapacity(int stadiumCapacity) {
        this.stadiumCapacity = stadiumCapacity;
    }

    public int getFounded() {
        return founded;
    }

    public void setFounded(int founded) {
        this.founded = founded;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public ArrayList<FootballPlayer> getPlayers() {
        return players;
    }

    public void setPlayers(ArrayList<FootballPlayer> players) {
        this.players = players;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(footballTeamName);
        parcel.writeString(officialName);
        parcel.writeString(url);
        parcel.writeString(logoUrl);
        parcel.writeString(email);
        parcel.writeString(website);
        parcel.writeString(address);
        parcel.writeString(coach);
        parcel.writeString(stadium);
        parcel.writeInt(stadiumCapacity);
        parcel.writeInt(founded);
        parcel.writeString(nickname);
        parcel.writeTypedList(players);
    }
}
