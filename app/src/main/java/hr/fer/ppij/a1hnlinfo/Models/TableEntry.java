/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hr.fer.ppij.a1hnlinfo.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 *
 * @author natko
 */
public class TableEntry implements Comparable<TableEntry>, Parcelable{
    private int position;
    private FootballTeam team;
    private int gamesPlayed;
    private int gamesWon;
    private int gamesDrawn;
    private int gamesLost;
    private int goalsFor;
    private int goalsAgainst;
    private int goalDifference;
    private int points;

    public TableEntry(int position, FootballTeam team, int gamesPlayed, int gamesWon, int gamesDrawn, int gamesLost, int goalsFor, int goalsAgainst, int goalDifference, int points) {
        this.position = position;
        this.team = team;
        this.gamesPlayed = gamesPlayed;
        this.gamesWon = gamesWon;
        this.gamesDrawn = gamesDrawn;
        this.gamesLost = gamesLost;
        this.goalsFor = goalsFor;
        this.goalsAgainst = goalsAgainst;
        this.goalDifference = goalDifference;
        this.points = points;
    }


    protected TableEntry(Parcel in) {
        position = in.readInt();
        team = in.readParcelable(FootballTeam.class.getClassLoader());
        gamesPlayed = in.readInt();
        gamesWon = in.readInt();
        gamesDrawn = in.readInt();
        gamesLost = in.readInt();
        goalsFor = in.readInt();
        goalsAgainst = in.readInt();
        goalDifference = in.readInt();
        points = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(position);
        dest.writeParcelable(team, flags);
        dest.writeInt(gamesPlayed);
        dest.writeInt(gamesWon);
        dest.writeInt(gamesDrawn);
        dest.writeInt(gamesLost);
        dest.writeInt(goalsFor);
        dest.writeInt(goalsAgainst);
        dest.writeInt(goalDifference);
        dest.writeInt(points);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TableEntry> CREATOR = new Creator<TableEntry>() {
        @Override
        public TableEntry createFromParcel(Parcel in) {
            return new TableEntry(in);
        }

        @Override
        public TableEntry[] newArray(int size) {
            return new TableEntry[size];
        }
    };

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public FootballTeam getTeam() {
        return team;
    }

    public void setTeam(FootballTeam team) {
        this.team = team;
    }

    public int getGamesPlayed() {
        return gamesPlayed;
    }

    public void setGamesPlayed(int gamesPlayed) {
        this.gamesPlayed = gamesPlayed;
    }

    public int getGamesWon() {
        return gamesWon;
    }

    public void setGamesWon(int gamesWon) {
        this.gamesWon = gamesWon;
    }

    public int getGamesDrawn() {
        return gamesDrawn;
    }

    public void setGamesDrawn(int gamesDrawn) {
        this.gamesDrawn = gamesDrawn;
    }

    public int getGamesLost() {
        return gamesLost;
    }

    public void setGamesLost(int gamesLost) {
        this.gamesLost = gamesLost;
    }

    public int getGoalsFor() {
        return goalsFor;
    }

    public void setGoalsFor(int goalsFor) {
        this.goalsFor = goalsFor;
    }

    public int getGoalsAgainst() {
        return goalsAgainst;
    }

    public void setGoalsAgainst(int goalsAgainst) {
        this.goalsAgainst = goalsAgainst;
    }

    public int getGoalDifference() {
        return goalDifference;
    }

    public void setGoalDifference(int goalsDifference) {
        this.goalDifference = goalsDifference;
    }
    
    

    @Override
    public int compareTo(TableEntry e) {
        return(position - e.position);
    }

    @Override
    public String toString() {
        return "TableEntry{" +
                "position=" + position +
                ", team=" + team.getFootballTeamName() +
                ", gamesPlayed=" + gamesPlayed +
                ", gamesWon=" + gamesWon +
                ", gamesDrawn=" + gamesDrawn +
                ", gamesLost=" + gamesLost +
                ", goalsFor=" + goalsFor +
                ", goalsAgainst=" + goalsAgainst +
                ", goalDifference=" + goalDifference +
                ", points=" + points +
                '}';
    }
}
