package hr.fer.ppij.a1hnlinfo.Models;

/**
 * Created by AndrejKovar on 28.3.2017..
 */

public class PlayerPosition {

    public static final String GOALKEEPER = "GK";
    public static final String DEFENDER = "DF";
    public static final String MIDFIELDER = "MF";
    public static final String STRIKER = "ST";

}
