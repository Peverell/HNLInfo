package hr.fer.ppij.a1hnlinfo.Networking;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import static java.lang.Boolean.FALSE;

/**
 * Helper class with custom regex functions suited for collecting
 * information from the text.
 *  
 */

public class RegexUtils {

    /**
     * Matches first occurrence of the first group of the given pattern.
     * @return First match or an empty string.
     */
    public static String getFirstMatch(String text, String pattern) {
        return getFirstMatch(text, pattern, FALSE);

    }
    
    /**
     * Matches first occurrence of the first group of the given pattern.
     * @param text String to be searched
     * @param pattern Pattern to be matched.
     * @return First match or an empty string.
     */
    public static String getFirstMatch(String text, String pattern, Boolean singleLine) {
        Pattern r;
        if (singleLine) {
            r = Pattern.compile(pattern,Pattern.DOTALL);
        }
        else {
            r = Pattern.compile(pattern);

        }
        Matcher m = r.matcher(text);
        if (m.find()) {
            return m.group(1);
        }
        return "";
    }
    
    /**
     * Returns an array of all matched groups of the first pattern match.
     * @param text String to be searched
     * @param pattern Pattern to be matched.
     * @return An array of matched groups or an empty array.
     */
    public static ArrayList<String> matchPattern(String text, String pattern){
        ArrayList<String> lista = new ArrayList<String>();
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(text);
        if (m.find()) {
            for (int i=1; i<=m.groupCount(); i++) {
                lista.add(m.group(i));
            }

        }
        return lista;
    }

    /**
     * Finds all occurrences of the pattern in the given text
     * and packs it in two dimensional array list.
     *
     * @param text String to be searched
     * @param pattern Pattern to be matched.
     * @return An array of arrays of matched groups or an empty array.
     */
    public static ArrayList<ArrayList<String>> matchAll(String text, String pattern) {
        return matchAll(text, pattern, FALSE);
    }

    /**
     * Finds all occurrences of the pattern in the given text
     * and packs it in two dimensional array list.
     * 
     * @param text String to be searched
     * @param pattern Pattern to be matched.
     * @return An array of arrays of matched groups or an empty array.
     */
    public static ArrayList<ArrayList<String>> matchAll(String text, String pattern, Boolean singleLine) {
        ArrayList<ArrayList<String>> my_array = new ArrayList<>();
        Pattern r;
        if (singleLine) {
            r = Pattern.compile(pattern,Pattern.DOTALL);
        }
        else {
            r = Pattern.compile(pattern);
        }
        Matcher m = r.matcher(text);
        ArrayList<String> pom;
        while (m.find()) {
            pom = new ArrayList<>();
            for (int i=1; i<=m.groupCount(); i++){
                pom.add(m.group(i));
            }
            
            my_array.add(pom);
        }
        return my_array;
    }

    /**
     * Gets the starting indexes of all pattern matches in the given text.
     * @param text String to be searched
     * @param pattern Pattern to be matched.
     * @return An array of indexes (integer).
     */
    public static ArrayList<Integer> getAllIndexes(String text, String pattern) {
        ArrayList<Integer> indexes = new ArrayList<>();
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(text);
        while (m.find()) {
            for (int i=1; i<=m.groupCount(); i++) {
                indexes.add(m.start(i));
            }

        }

        return indexes;
    }

    /**
     * Gets the starting index of the first pattern match in the given text.
     * @param text String to be searched
     * @param pattern Pattern to be matched.
     * @return Index (integer).
     */
    public static int getFirstIndex(String text, String pattern) {
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(text);
        if (m.find()) {
            return m.start(1);
        }
        return 0;
    }
}
