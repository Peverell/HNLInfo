package hr.fer.ppij.a1hnlinfo.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import hr.fer.ppij.a1hnlinfo.Activites.FootballTeamActivity;
import hr.fer.ppij.a1hnlinfo.Activites.MainActivity;
import hr.fer.ppij.a1hnlinfo.Adapters.TeamsAdapter;
import hr.fer.ppij.a1hnlinfo.Models.FootballTeam;
import hr.fer.ppij.a1hnlinfo.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TeamsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TeamsFragment extends Fragment {

    public static final String TAG = "teams_fragment";
    public static final int ID = 2;

    @BindView(R.id.rv_teams)
    RecyclerView rvTeams;

    public TeamsFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment.
     *
     * @return A new instance of fragment TeamsFragment.
     */
    public static TeamsFragment newInstance() {
        TeamsFragment fragment = new TeamsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_teams, container, false);
        ButterKnife.bind(this, view);

        viewInitialization();

        return view;
    }

    private void viewInitialization() {

        TeamsAdapter teamsAdapter = new TeamsAdapter(
                getContext(),
                ((MainActivity) getActivity()).getTeams(),
                (footballTeam) -> {
                    FootballTeam clickedFootballTeam = new FootballTeam(footballTeam);
                    startActivity(FootballTeamActivity.getIntent(getActivity().getApplicationContext(), clickedFootballTeam));
                }
        );
        rvTeams.setAdapter(teamsAdapter);
    }

}
