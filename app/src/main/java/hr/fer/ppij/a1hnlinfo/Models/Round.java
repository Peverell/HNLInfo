package hr.fer.ppij.a1hnlinfo.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by natko on 06.05.17..
 */

public class Round implements Parcelable {
    private final int number;
    private final String text;

    public Round(int number, String text) {
        this.number = number;
        this.text = text;
    }

    public int getNumber() {
        return number;
    }

    public String getText() {
        return text;
    }

    protected Round(Parcel in) {
        number = in.readInt();
        text = in.readString();
    }

    public static final Creator<Round> CREATOR = new Creator<Round>() {
        @Override
        public Round createFromParcel(Parcel in) {
            return new Round(in);
        }

        @Override
        public Round[] newArray(int size) {
            return new Round[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeInt(number);
        parcel.writeString(text);

    }

    @Override
    public String toString() {
        return text;
    }
}
