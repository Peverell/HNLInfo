package hr.fer.ppij.a1hnlinfo.Activites;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import hr.fer.ppij.a1hnlinfo.Models.FootballTeam;
import hr.fer.ppij.a1hnlinfo.Models.Match;
import hr.fer.ppij.a1hnlinfo.Models.Round;
import hr.fer.ppij.a1hnlinfo.Models.TableEntry;
import hr.fer.ppij.a1hnlinfo.Networking.DataCollector;
import hr.fer.ppij.a1hnlinfo.R;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.wang.avi.AVLoadingIndicatorView;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;

public class SplashActivity extends Activity {

    @BindView(R.id.progressBar_splash_screen)
    AVLoadingIndicatorView spinner;
    @BindView(R.id.tv_splash_downloading)
    TextView splashDownloadingTV;

    ArrayList<FootballTeam> teams;
    ArrayList<Match> matches;
    ArrayList<TableEntry> table;
    ArrayList<Round> rounds;

    private boolean shouldExecuteOnResume;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        shouldExecuteOnResume = false;

        Context context = getApplicationContext();
        SharedPreferences mPrefs = context.getSharedPreferences("HNLCache", Context.MODE_PRIVATE);
        Gson gson = new Gson();

        String json = mPrefs.getString("time", "");
        Type type = new TypeToken<Long>() {}.getType();
        Long time = gson.fromJson(json, type);
        if (time == null){
            time = 0l;
        }
        Long now = System.currentTimeMillis() / 1000l;
        if ((now - time) > 1800 ){ //30 minuta
            Log.e("TEST", "ULAZI u download");
            startDownloading();
        }else{
            Intent intent = new Intent(SplashActivity.this, MainActivity.class);
            startActivity(intent);
        }
        //startDownloading();

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (shouldExecuteOnResume) {
            Context context = getApplicationContext();
            SharedPreferences mPrefs = context.getSharedPreferences("HNLCache", Context.MODE_PRIVATE);
            Gson gson = new Gson();

            String json = mPrefs.getString("time", "");
            Type type = new TypeToken<Long>() {}.getType();
            Long time = gson.fromJson(json, type);
            if (time == null) {
                time = 0l;
            }
            Long now = System.currentTimeMillis() / 1000l;
            if ((now - time) > 1800) { //30 minuta
                Log.e("TEST", "ULAZI u download");
                startDownloading();
            }
        }
    }

    private void startDownloading(){

        spinner.smoothToShow();
        splashDownloadingTV.setTextColor(getResources().getColor(R.color.white));
        new DownloadFiles().execute();
    }

    private class DownloadFiles extends AsyncTask<Void, String, Boolean>{

        /** This method downloads all necessary data.
         *
         * @param voids no arguments
         * @return true if any error occurred
         */
        @Override
        protected Boolean doInBackground(Void... voids) {

            try {
                publishProgress(getResources().getString(R.string.teams_downloading));
                teams = DataCollector.getTeams();

                publishProgress(getResources().getString(R.string.matches_downloading));
                ArrayList<FootballTeam> t2 = new ArrayList<>();
                t2.addAll(teams);
                matches = DataCollector.getMatches(t2);
                rounds = (ArrayList<Round>) DataCollector.getRounds();

                publishProgress(getResources().getString(R.string.table_downloading));
                table = DataCollector.getTable(t2);






            } catch (IOException e) {
                Log.e("Error", e.toString());
                e.printStackTrace();
                publishProgress(getResources().getString(R.string.internet_connection_problem));
                return true;
            }
            publishProgress(getResources().getString(R.string.putting_into_cache));

            Context context = getApplicationContext();
            SharedPreferences mPrefs = context.getSharedPreferences("HNLCache", Context.MODE_PRIVATE);
            SharedPreferences.Editor prefsEditor = mPrefs.edit();
            Gson gson = new Gson();
            Type type;

            type = new TypeToken<ArrayList<FootballTeam>>() {}.getType();
            String json = gson.toJson(teams, type);
            prefsEditor.putString("teams", json);

            type = new TypeToken<ArrayList<Match>>() {}.getType();
            json = gson.toJson(matches, type);
            prefsEditor.putString("matches", json);

            type = new TypeToken<ArrayList<Round>>() {}.getType();
            json = gson.toJson(rounds, type);
            prefsEditor.putString("rounds", json);

            type = new TypeToken<ArrayList<TableEntry>>() {}.getType();
            json = gson.toJson(table, type);
            prefsEditor.putString("table", json);

            type = new TypeToken<Long>() {}.getType();
            Long time = System.currentTimeMillis() / 1000l;
            json = gson.toJson(time, type);
            prefsEditor.putString("time", json);

            prefsEditor.commit();

            return false;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            setSplashText(values[0]);
        }

        @Override
        protected void onPostExecute(Boolean errorOccurred) {

            Log.e("DOWNLOADING: ",  "Error occurred: " + errorOccurred.toString());

            //internet connection error occurred
            if(errorOccurred) {

                splashDownloadingTV.setTextColor(getResources().getColor(R.color.colorAccent));
                shouldExecuteOnResume = true;

            } else {

                splashDownloadingTV.setVisibility(View.GONE);
                spinner.smoothToHide();

                //starting main activity
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
//                intent.putParcelableArrayListExtra(MainActivity.TEAMS_EXTRA, teams);
//                intent.putParcelableArrayListExtra(MainActivity.MATCHES_EXTRA, matches);
//                intent.putParcelableArrayListExtra(MainActivity.TABLE_EXTRA, table);
//                intent.putParcelableArrayListExtra(MainActivity.ROUNDS_EXTRA, rounds);
                startActivity(intent);
                finish();
            }
        }
    }

    private void setSplashText(String text){
        splashDownloadingTV.setText(text);
    }

    private void showToast(int string){
        Toast.makeText(SplashActivity.this, string, Toast.LENGTH_LONG).show();
    }
}
