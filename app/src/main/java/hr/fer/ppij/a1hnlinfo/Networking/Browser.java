
package hr.fer.ppij.a1hnlinfo.Networking;

import org.jsoup.Jsoup;

import java.io.IOException;

/**
 * Simulates a simple browser for getting HTML source.
 * @author natko
 */
public class Browser {
    
    /**
     * Get HTML from a given url.
     * @param url URL to be visited.
     * @return HTML response from the url 
     */
    public static String getHTML(String url, String referer) throws IOException {


        //TODO no internet connection ?
        String html = "";

        html = Jsoup.connect(url).referrer(referer).userAgent(DataConstants.USER_AGENT).timeout(3000).get().html();

        return html;
        
    }

    /**
     * Get HTML from a given url.
     * @param url URL to be visited.
     * @return HTML response from the url
     */
    public static String getHTML(String url) throws IOException {


        return getHTML(url, "http://google.hr");

    }
    
}
