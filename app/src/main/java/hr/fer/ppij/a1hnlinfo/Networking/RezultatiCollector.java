package hr.fer.ppij.a1hnlinfo.Networking;

import java.io.IOException;
import java.util.ArrayList;

import hr.fer.ppij.a1hnlinfo.Models.FootballPlayer;
import hr.fer.ppij.a1hnlinfo.Models.PlayerPosition;

import static java.lang.Boolean.TRUE;

/**
 * Created by natko on 04.05.17..
 */

public class RezultatiCollector {

    public static ArrayList<FootballPlayer> getPlayers (String tName) throws IOException {
        ArrayList<FootballPlayer> teamPlayers = new ArrayList<>();

        String url = DataConstants.REZULTATI_TEAM_URL.get(tName);

        String html = Browser.getHTML(url + "/momcad/");

        ArrayList<ArrayList<String>> players = RegexUtils.matchAll(html, "class=[\\\"\\']player.+?<td.+?>([A-Za-z ]+)<\\/a><\\/td>\\s*.+?>(\\d+|\\?)<.+?>(\\d+)<.+?>(\\d+)<.+?>(\\d+)<.+?>(\\d+)<", TRUE);

        //determine player position
        ArrayList<Integer> pom = RegexUtils.getAllIndexes(html, "(class=[\\\"\\']player-type-title[\\\"\\'])");
        int goalkeeperIndex = Integer.valueOf(pom.get(0));
        int defenderIndex = Integer.valueOf(pom.get(1));
        int midfielderIndex = Integer.valueOf(pom.get(2));
        int forwardIndex = Integer.valueOf(pom.get(3));


        String position;
        for (ArrayList<String> p : players) {
            String pName = p.get(0);
            int pomIndex = html.indexOf(String.format(">%s<",pName));

            position = PlayerPosition.GOALKEEPER;

            if (pomIndex > defenderIndex) {
                position = PlayerPosition.DEFENDER;
            }
            if (pomIndex > midfielderIndex) {
                position = PlayerPosition.MIDFIELDER;
            }
            if (pomIndex > forwardIndex) {
                position = PlayerPosition.STRIKER;
            }
            //initialize other variables
            String[] arr = pName.split(" ");
            String lastname = arr[0];
            String firstname = pName.replaceFirst(lastname,""); firstname.trim();

            int appearances = Integer.valueOf(p.get(2));
            int years = Integer.valueOf(p.get(1).replace("?","0"));
            int goals = Integer.valueOf(p.get(3));
            int yellowCards = Integer.valueOf(p.get(4));
            int redCards = Integer.valueOf(p.get(5));

            FootballPlayer player = new FootballPlayer(firstname, lastname, appearances, years, goals, yellowCards, redCards, position);
            teamPlayers.add(player);

        }
        return teamPlayers;

    }
}
