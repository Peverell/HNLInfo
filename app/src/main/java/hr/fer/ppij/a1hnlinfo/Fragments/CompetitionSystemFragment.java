package hr.fer.ppij.a1hnlinfo.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import hr.fer.ppij.a1hnlinfo.R;

/**
 * Created by domin on 10/05/2017.
 */

public class CompetitionSystemFragment extends Fragment {

    public static final String TAG = "hnl_history_fragmen";

    public CompetitionSystemFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment.
     *
     * @return A new instance of fragment MatchesFragment.
     */
    public static CompetitionSystemFragment newInstance() {
        CompetitionSystemFragment fragment = new CompetitionSystemFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.competition_system, container, false);
    }

}
