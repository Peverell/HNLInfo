package hr.fer.ppij.a1hnlinfo.Fragments;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import hr.fer.ppij.a1hnlinfo.Activites.MainActivity;
import hr.fer.ppij.a1hnlinfo.Adapters.MatchAdapter;
import hr.fer.ppij.a1hnlinfo.Models.Match;
import hr.fer.ppij.a1hnlinfo.Models.Round;
import hr.fer.ppij.a1hnlinfo.Networking.DataCollector;
import hr.fer.ppij.a1hnlinfo.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MatchesFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MatchesFragment extends Fragment {

    public static final String TAG = "matches_fagment";
    public static final int ID = 0;

    @BindView(R.id.spinner)
    Spinner spinner;
    @BindView(R.id.rv_matches)
    RecyclerView rvMatches;

    public MatchesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment.
     *
     * @return A new instance of fragment MatchesFragment.
     */
    public static MatchesFragment newInstance() {
        MatchesFragment fragment = new MatchesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_matches, container, false);
        ButterKnife.bind(this, view);

        roundsInit();
        return view;
    }

    private void roundsInit(){
        Context context = getContext();
        ArrayList<Round> rounds;
        rounds = ((MainActivity)getActivity()).getRounds();
        int lastRound = rounds.size();
        for (int i=lastRound+1; i<=36; i++){
            rounds.add(0, new Round(i, String.format("%d. KOLO", i)));
        }
        ArrayAdapter<Round> adapter = new ArrayAdapter<>(getActivity(),R.layout.spinner_text, rounds);
        adapter.setDropDownViewResource(R.layout.spinner_dropdown);
        spinner.setAdapter(adapter);

        spinner.setSelection(36-lastRound);


        spinner.setOnItemSelectedListener(new CustomOnItemSelectedListener());
    }

    private class CustomOnItemSelectedListener implements android.widget.AdapterView.OnItemSelectedListener {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            Context context = getContext();
            Round round = (Round) parent.getItemAtPosition(position);
            ArrayList<Match> ms = ((MainActivity)getActivity()).getMatches();
            ArrayList<Match> matches = DataCollector.getMatchesByRound(round, ms);
            MatchAdapter adapter = new MatchAdapter(context, matches);
            rvMatches.setAdapter(adapter);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    }
}
