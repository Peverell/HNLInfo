package hr.fer.ppij.a1hnlinfo.Networking;

import java.util.HashMap;
import java.util.Map;

/**
 * Class with constant values.
 */

public final class DataConstants {
    /**
     * Basic site domain variable.
     */
    public static final String DOMAIN = "http://prvahnl.hr";
    
    /**
     * URL with matches information.
     */
    public static final String MATCHES_URL = DOMAIN + "/prva-liga/raspored-i-rezultati/";
    
    /**
     * URL with table information.
     */
    public static final String TABLE_URL = DOMAIN + "/prva-liga/ljestvica/";
    
    /**
     * Date format for converting date string to Date object.
     */
    public static final String MATCH_DATE_FORMAT = "dd.MM.yyyy.";

    public static final String USER_AGENT = "Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6";

    public static final Map<String, String> REZULTATI_TEAM_URL = new HashMap<String , String>() {
        {
            put("Cibalia", "http://www.rezultati.com/tim/cibalia/Q7rTf6Em");
            put("Dinamo", "http://www.rezultati.com/tim/din-zagreb/8G5ufQTg");
            put("Hajduk", "http://www.rezultati.com/tim/hajduk-split/2w5qgpq0");
            put("Inter-Zaprešić", "http://www.rezultati.com/tim/inter-z/zJX3Z3Da");
            put("Istra 1961", "http://www.rezultati.com/tim/istra-1961/hCg2iHl4");
            put("Lokomotiva", "http://www.rezultati.com/tim/lok-zagreb/CWwBJ6PA");
            put("Osijek", "http://www.rezultati.com/tim/osijek/448T3rTI");
            put("Rijeka", "http://www.rezultati.com/tim/rijeka/zXKuUR5J");
            put("Split", "http://www.rezultati.com/tim/rnk-split/hGMWMdT0");
            put("Slaven Belupo", "http://www.rezultati.com/tim/slaven-belupo/ENJqToLP");
        }};

    public static final String REZULTATI_URL = "http://www.rezultati.com/nogomet/hrvatska/1-hnl/rezultati/";
}
