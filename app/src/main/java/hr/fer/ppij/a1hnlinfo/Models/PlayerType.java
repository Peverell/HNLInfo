package hr.fer.ppij.a1hnlinfo.Models;

/**
 * Created by natko on 25.04.17..
 */

public class PlayerType {
    public static final String STARTER = "Starter";
    public static final String SUBSTITUTE = "Substitute";
}
