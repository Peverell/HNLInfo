package hr.fer.ppij.a1hnlinfo.Models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Class representing the match result with the number of goals for each team. 
 */

public class MatchResults implements Parcelable{
    private Integer homeTeamGoals;
    private Integer  awayTeamGoals;


    /**
     * Constructor that sets variables to the given values.
     * @param homeTeamGoals
     * @param awayTeamGoals 
     */
    public MatchResults(int homeTeamGoals, int awayTeamGoals) {
        this.setHomeTeamGoals(homeTeamGoals);
        this.setAwayTeamGoals(awayTeamGoals);
    }

    /**
    * Empty constructor initializes variables to null.    
    */
    public MatchResults() {
        this.setAwayTeamGoals(null);
        this.setHomeTeamGoals(null);
    }

    public MatchResults(MatchResults rs) {
        this.setAwayTeamGoals(rs.getAwayTeamGoals());
        this.setHomeTeamGoals(rs.getHomeTeamGoals());
    }




    protected MatchResults(Parcel in) {
        in.readInt();
        in.readInt();
    }

    public static final Creator<MatchResults> CREATOR = new Creator<MatchResults>() {
        @Override
        public MatchResults createFromParcel(Parcel in) {
            return new MatchResults(in);
        }

        @Override
        public MatchResults[] newArray(int size) {
            return new MatchResults[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(homeTeamGoals);
        parcel.writeInt(awayTeamGoals);
    }

    public Integer getHomeTeamGoals() {
        return homeTeamGoals;
    }

    public void setHomeTeamGoals(Integer homeTeamGoals) {
        this.homeTeamGoals = homeTeamGoals;
    }

    public Integer getAwayTeamGoals() {
        return awayTeamGoals;
    }

    public void setAwayTeamGoals(Integer awayTeamGoals) {
        this.awayTeamGoals = awayTeamGoals;
    }

    @Override
    public String toString() {
        return "MatchResults{" +
                "homeTeamGoals=" + homeTeamGoals +
                ", awayTeamGoals=" + awayTeamGoals +
                '}';
    }
}
