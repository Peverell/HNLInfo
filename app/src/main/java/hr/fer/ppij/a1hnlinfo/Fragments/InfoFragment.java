package hr.fer.ppij.a1hnlinfo.Fragments;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import hr.fer.ppij.a1hnlinfo.Adapters.InfoAdapter;
import butterknife.ButterKnife;
import hr.fer.ppij.a1hnlinfo.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link InfoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class InfoFragment extends Fragment {

    public static final String TAG = "info_fragment";
    public static final int ID = 3;

    PagerAdapter adapter;

    View view;

    public InfoFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment.
     *
     * @return A new instance of fragment InfoFragment.
     */
    public static InfoFragment newInstance() {
        InfoFragment fragment = new InfoFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_info, container, false);
        super.onCreate(savedInstanceState);

        //ButterKnife.bind(this, view);

        final ViewPager viewPager = ButterKnife.findById(view, R.id.viewpager);

        TabLayout tabLayout = ButterKnife.findById(view, R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        viewInitialization(viewPager);

        return view;

    }

    private void viewInitialization(ViewPager viewPager) {
        InfoAdapter adapter = new InfoAdapter(getChildFragmentManager());
        adapter.addFragment(new HnlHistoryFragmen(),"Povijest ");
        adapter.addFragment(new CompetitionSystemFragment(),"Tip natjecanja");
        adapter.addFragment(new AppInfoFragment(),"O aplikaciji");
        viewPager.setAdapter(adapter);
    }




}
