package hr.fer.ppij.a1hnlinfo.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by domin on 10/05/2017.
 */

public class InfoAdapter extends FragmentPagerAdapter{

    private final List<android.support.v4.app.Fragment> infoFragments = new ArrayList<>();
    private final List<String> infoFragmentsTitels = new ArrayList<>();

    public InfoAdapter(FragmentManager fragmentManagerm){
        super(fragmentManagerm);
    }

    public void addFragment(android.support.v4.app.Fragment infoFragment, String name){
        infoFragments.add(infoFragment);
        infoFragmentsTitels.add(name);
    }

    @Override
    public Fragment getItem(int position) {
        return infoFragments.get(position);
    }

    @Override
    public int getCount() {
        return infoFragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position){
        return infoFragmentsTitels.get(position);
    }
}
