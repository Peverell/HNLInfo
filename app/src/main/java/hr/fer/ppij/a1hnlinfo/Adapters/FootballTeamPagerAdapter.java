package hr.fer.ppij.a1hnlinfo.Adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import hr.fer.ppij.a1hnlinfo.Fragments.TeamInfoFragment;
import hr.fer.ppij.a1hnlinfo.Fragments.TeamPlayersFragment;

/**
 * Created by AndrejKovar on 16.4.2017..
 */

public class FootballTeamPagerAdapter extends FragmentPagerAdapter {

    private int numOfTabs;

    public FootballTeamPagerAdapter(FragmentManager fragmentManager, int numOfTabs){
        super(fragmentManager);
        this.numOfTabs = numOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return new TeamInfoFragment();
            case 1:
                return new TeamPlayersFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numOfTabs;
    }
}
