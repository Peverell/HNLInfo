package hr.fer.ppij.a1hnlinfo.Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Created by AndrejKovar on 28.3.2017..
 */

public class Match implements Parcelable{

    private int matchWeek;
    private Date matchDate;
    private String matchStadium;
    private String matchCity;
    private FootballTeam homeTeam;
    private FootballTeam awayTeam;
    private MatchResults matchResults;
    private String matchUrl;

    public Match(
            int matchWeek,
            Date matchDate,
            String matchCity,
            String matchStadium,
            FootballTeam homeTeam,
            FootballTeam awayTeam,
            MatchResults matchResults,
            String matchUrl) {

        this.matchCity = matchCity;
        this.matchWeek = matchWeek;
        this.matchDate = matchDate;
        this.matchStadium = matchStadium;
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        this.matchResults = matchResults;
        this.matchUrl = matchUrl;
    }

    public Match (Match match) {
        this.matchWeek = match.getMatchWeek();
        this.matchDate = match.getMatchDate();
        this.matchStadium = match.getMatchStadium();
        this.matchCity = match.getMatchCity();
        this.homeTeam = match.getHomeTeam();
        this.awayTeam = match.getAwayTeam();
        this.matchResults = match.getMatchResults();
        this.matchUrl = match.getMatchUrl();
    }

    protected Match(Parcel in) {
        matchWeek = in.readInt();
        matchStadium = in.readString();
        matchCity = in.readString();
        matchUrl = in.readString();
        homeTeam = in.readParcelable(getClass().getClassLoader());
        awayTeam = in.readParcelable(getClass().getClassLoader());
        matchDate = (Date) in.readSerializable();
        matchResults = in.readParcelable(getClass().getClassLoader());
    }

    public static final Creator<Match> CREATOR = new Creator<Match>() {
        @Override
        public Match createFromParcel(Parcel in) {
            return new Match(in);
        }

        @Override
        public Match[] newArray(int size) {
            return new Match[size];
        }
    };

    public String getMatchUrl() {
        return matchUrl;
    }

    public void setMatchUrl(String matchUrl) {
        this.matchUrl = matchUrl;
    }
    
    

    public int getMatchWeek() {
        return matchWeek;
    }

    public void setMatchWeek(int matchWeek) {
        this.matchWeek = matchWeek;
    }

    public Date getMatchDate() {
        return matchDate;
    }

    public void setMatchDate(Date matchDate) {
        this.matchDate = matchDate;
    }

    public String getMatchStadium() {
        return matchStadium;
    }

    public void setMatchStadium(String matchStadium) {
        this.matchStadium = matchStadium;
    }

    public String getMatchCity() {
        return matchCity;
    }

    public void setMatchCity(String matchCity) {
        this.matchCity = matchCity;
    }

    public FootballTeam getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(FootballTeam homeTeam) {
        this.homeTeam = homeTeam;
    }

    public FootballTeam getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(FootballTeam awayTeam) {
        this.awayTeam = awayTeam;
    }

    public MatchResults getMatchResults() {
        return matchResults;
    }

    public void setMatchResults(MatchResults matchResults) {
        this.matchResults = matchResults;
    }

    @Override
    public String toString() {
        return homeTeam.getFootballTeamName() + " - " + awayTeam.getFootballTeamName();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(matchWeek);
        parcel.writeString(matchStadium);
        parcel.writeString(matchCity);
        parcel.writeString(matchUrl);
        parcel.writeParcelable(homeTeam,i);
        parcel.writeParcelable(awayTeam,i);
        parcel.writeSerializable(matchDate);
        parcel.writeParcelable(matchResults,i);
    }
}
