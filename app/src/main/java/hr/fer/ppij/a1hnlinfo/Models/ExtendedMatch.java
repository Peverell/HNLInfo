package hr.fer.ppij.a1hnlinfo.Models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by natko on 25.04.17..
 */

public class ExtendedMatch implements Parcelable {
    private Match match;
    private String description;
    private ArrayList<ExtendedFootballPlayer> homeLineup;
    private ArrayList<ExtendedFootballPlayer> awayLineup;

    public ExtendedMatch(Match match, String description, ArrayList<ExtendedFootballPlayer> homeLineup, ArrayList<ExtendedFootballPlayer> awayLineup) {
        this.match = match;
        this.description = description;
        this.homeLineup = homeLineup;
        this.awayLineup = awayLineup;
    }

    public ExtendedMatch (Parcel in) {
        match = in.readParcelable(getClass().getClassLoader());
        description = in.readString();
        homeLineup = in.createTypedArrayList(ExtendedFootballPlayer.CREATOR);
        awayLineup = in.createTypedArrayList(ExtendedFootballPlayer.CREATOR);
    }

    public static final Creator<ExtendedMatch> CREATOR = new Creator<ExtendedMatch>() {
        @Override
        public ExtendedMatch createFromParcel(Parcel in) {
            return new ExtendedMatch(in);
        }

        @Override
        public ExtendedMatch[] newArray(int size) {
            return new ExtendedMatch[size];
        }
    };

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Match getMatch() {
        return match;
    }

    public void setMatch(Match match) {
        this.match = match;
    }

    public ArrayList<ExtendedFootballPlayer> getHomeLineup() {
        return homeLineup;
    }

    public void setHomeLineup(ArrayList<ExtendedFootballPlayer> homeLineup) {
        this.homeLineup = homeLineup;
    }

    public ArrayList<ExtendedFootballPlayer> getAwayLineup() {
        return awayLineup;
    }

    public void setAwayLineup(ArrayList<ExtendedFootballPlayer> awayLineup) {
        this.awayLineup = awayLineup;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeParcelable(match, 0);
        parcel.writeString(description);
        parcel.writeTypedList(homeLineup);
        parcel.writeTypedList(awayLineup);
    }

    @Override
    public String toString() {
        return "ExtendedMatch{" +
                "match=" + match +
                ", description='" + description + '\'' +
                ", homeLineup=" + homeLineup +
                ", awayLineup=" + awayLineup +
                '}';
    }
}