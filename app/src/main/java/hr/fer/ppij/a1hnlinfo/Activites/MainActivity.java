package hr.fer.ppij.a1hnlinfo.Activites;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import hr.fer.ppij.a1hnlinfo.Fragments.InfoFragment;
import hr.fer.ppij.a1hnlinfo.Fragments.MatchesFragment;
import hr.fer.ppij.a1hnlinfo.Fragments.TableFragment;
import hr.fer.ppij.a1hnlinfo.Fragments.TeamsFragment;
import hr.fer.ppij.a1hnlinfo.Models.FootballTeam;
import hr.fer.ppij.a1hnlinfo.Models.Match;
import hr.fer.ppij.a1hnlinfo.Models.Round;
import hr.fer.ppij.a1hnlinfo.Models.TableEntry;
import hr.fer.ppij.a1hnlinfo.R;

public class MainActivity extends AppCompatActivity {

    public static String TEAMS_EXTRA = "TEAMS_EXTRA";
    public static String MATCHES_EXTRA = "MATCHES_EXTRA";
    public static String TABLE_EXTRA = "TABLE_EXTRA";
    public static String ROUNDS_EXTRA = "ROUNDS_EXTRA";

    private ArrayList<FootballTeam> teams;
    private ArrayList<Match> matches;
    private ArrayList<TableEntry> table;
    private ArrayList<Round> rounds;

    private int activeFragmentID;
    private String activeFragmentBundleKey = "activeFragmetID";

    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigationView;

    @BindView(R.id.activity_main_toolbar)
    Toolbar mainToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        //loading from cache
        Context context = getApplicationContext();
        SharedPreferences mPrefs = context.getSharedPreferences("HNLCache", Context.MODE_PRIVATE);
        Gson gson = new Gson();

        String json = mPrefs.getString("teams", "");
        Type type = new TypeToken<ArrayList<FootballTeam>>() {}.getType();
        teams = gson.fromJson(json, type);

        json = mPrefs.getString("matches", "");
        type = new TypeToken<ArrayList<Match>>() {}.getType();
        matches = gson.fromJson(json, type);

        json = mPrefs.getString("table", "");
        type = new TypeToken<ArrayList<TableEntry>>() {}.getType();
        table = gson.fromJson(json, type);

        json = mPrefs.getString("rounds", "");
        type = new TypeToken<ArrayList<Round>>() {}.getType();
        rounds = gson.fromJson(json, type);


        mainToolbar.setTitle(getResources().getString(R.string.app_name));
        mainToolbar.setTitleTextColor(getResources().getColor(R.color.white));

        if(savedInstanceState != null){

            activeFragmentID = savedInstanceState.getInt(activeFragmentBundleKey);

            switch (activeFragmentID){

                case MatchesFragment.ID:
                    bottomNavigationView.setSelectedItemId(R.id.action_results);
                    break;

                case TableFragment.ID:
                    bottomNavigationView.setSelectedItemId(R.id.action_table);
                    break;

                case TeamsFragment.ID:
                    bottomNavigationView.setSelectedItemId(R.id.action_teams);
                    break;

                case InfoFragment.ID:
                    bottomNavigationView.setSelectedItemId(R.id.action_info);
                    break;

                default:
                    bottomNavigationView.setSelectedItemId(R.id.action_results);
            }
        } else {

            replaceFragment(
                    MatchesFragment.newInstance(),
                    MatchesFragment.TAG,
                    R.id.fragment_container
            );
            activeFragmentID = MatchesFragment.ID;
        }

        bottomNavigationViewInit();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(activeFragmentBundleKey, activeFragmentID);
    }

    private void bottomNavigationViewInit(){

        bottomNavigationView.setOnNavigationItemSelectedListener((item) -> {

            switch (item.getItemId()) {

                case R.id.action_results:
                    //TODO dovršiti fragment resporeda i rezulrata 17.4.2017.
                    replaceFragment(
                            MatchesFragment.newInstance(),
                            MatchesFragment.TAG,
                            R.id.fragment_container
                    );
                    activeFragmentID = MatchesFragment.ID;
                    break;

                case R.id.action_table:
                    replaceFragment(
                            TableFragment.newInstance(),
                            TableFragment.TAG,
                            R.id.fragment_container
                    );
                    activeFragmentID = TableFragment.ID;
                    break;

                case R.id.action_teams:
                    replaceFragment(
                            TeamsFragment.newInstance(),
                            TeamsFragment.TAG,
                            R.id.fragment_container
                    );
                    activeFragmentID = TeamsFragment.ID;
                    break;

                case R.id.action_info:
                    replaceFragment(
                            InfoFragment.newInstance(),
                            InfoFragment.TAG,
                            R.id.fragment_container
                    );
                    activeFragmentID = InfoFragment.ID;
                    break;

                default:
                    replaceFragment(
                            MatchesFragment.newInstance(),
                            MatchesFragment.TAG,
                            R.id.fragment_container
                    );
                    activeFragmentID = MatchesFragment.ID;
            }
            return true;
        });
    }

    protected void replaceFragment(Fragment fragment, String tag, int fragmentContainerID) {

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out);
        transaction.replace(fragmentContainerID, fragment, tag).commit();

        getSupportFragmentManager().executePendingTransactions();
    }

    public ArrayList<FootballTeam> getTeams(){
        return teams;
    }

    public ArrayList<Match> getMatches(){
        return matches;
    }

    public ArrayList<TableEntry> getTable(){
        return table;
    }

    public ArrayList<Round> getRounds(){
        return rounds;
    }


}
